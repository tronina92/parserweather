import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Patterns {

    private static Pattern patForDate = Pattern.compile("\\d{2}\\.\\d{2}");

    private String getDateFromString(String stringDate) throws Exception {
        Matcher matcher = patForDate.matcher(stringDate);
        if (matcher.find()) {
            return matcher.group();
        }
        throw new Exception("Can't extract date from string!");
    }

    ArrayList<String> getDate(Elements names, String str) throws Exception { //получение дат из элементов
        ArrayList<String> dates = new ArrayList<String>();
        for (Element name : names) {
            String dateString = name.select(str).text();
            dates.add(getDateFromString(dateString)); //получаем список дат
        }
        return dates;
    }
}
