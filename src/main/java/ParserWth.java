import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;

public class ParserWth {

    Document getPage() throws IOException { //получить страницу
        String url = "http://www.pogoda.spb.ru/";
        Document page = Jsoup.parse(new URL(url), 3000);
        return page;
    }

    Element getTable(Document page, String str) { //получить 1-ю таблицу со страницы
        return page.select(str).first();
    }

    Elements getValues (Element e, String str) { //получить значения
        return e.select(str);
    }

    String [][] getValueArray(Elements values, String str, int colomns) { // получить массив значений

        String valueLine[][] = new String[values.size()][colomns];
        int count = 0;
        for (Element value : values) {
            int count2=0;
            String strValues [] = new String[values.size()];
            Elements td = value.select(str);

            for (Element t:td){
                strValues[count] = t.text();
                valueLine[count][count2] = strValues[count];
                count2++;
            }
            count++;
        }
        return valueLine;
    }
}
