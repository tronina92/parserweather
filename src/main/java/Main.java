import org.jsoup.nodes.*;
import org.jsoup.select.Elements;

import javax.swing.*;
import java.util.ArrayList;

public class Main {

    Document page; //страница, с которой будем брать данные
    Element table; //таблица с page
    Elements names; //заголовки в таблице с page
    Elements values; //значения в таблице с page
    ArrayList<String> dates;

    private String[][] array; //массив данных без заголовков
    private String[] columnsHeader; //заголовок для JTable
    ArrayList<String[]> cH = new ArrayList<String[]>(); //список заголовков
    String [][] data; //массив данных для таблицы: cH + array

    public static void main(String[] args) throws Exception {
        Main main =new Main();
        main.createGUI();
    }

    public void createValues() throws Exception { //подготовка даных для таблицы

        ParserWth parserWth = new ParserWth();
        page = parserWth.getPage();
        table = parserWth.getTable(page, "table[class=wt]");
        names = parserWth.getValues(table, "tr[class=wth]"); //получаем заголовки из таблицы
        values = parserWth.getValues(table, "tr[valign=top]");//получаем значения: температура, ветер и т д

        Patterns patternDate = new Patterns();
        dates = patternDate.getDate(names, "th[id=dt]");//получаем даты из заголовков names

        int count = 0;
        //формируем заголовки
        for (Element name : names){
            columnsHeader = new String[]{dates.get(count), "Явления", "Температура", "Давление", "Влажность", "Ветер"};
            cH.add(columnsHeader);
            count++;
        }

        array = parserWth.getValueArray(values, "td", 6); //формируем массив значений погоды

        int numberOfrows = (array.length)+(cH.size()); //всего строк в итоговой таблице

        data = new String[numberOfrows][columnsHeader.length]; //полная таблица: заголовки+значения погоды

        int coun = 0; //счетчик строк заголовков
        int coun2 = 0; //счетчик строк значений погоды

        for(int i=0; i<(numberOfrows);i++) {
            //вывод заголовка
            if (i==0||array[coun2][0].contains("Утро")){
                for (int j=0; j<columnsHeader.length;j++) {
                    String[] str = cH.get(coun);
                    data[i][j] = str[j];
                }
                coun++;
                i++;
            }
            //вывод значений
            for (int n=0; n<columnsHeader.length;n++){
                data[i][n] = array[coun2][n];
                if (n == columnsHeader.length-1) coun2++;
            }
        }
    }

    public void createGUI() throws Exception {

        createValues();
        JFrame frame = new JFrame();

        JTable table1 = new JTable(data, columnsHeader);
        frame.add(table1);
        frame.setSize(600,600);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


}

